package nl.utwente.di.CelsiusFahrenheit;

public class Calculator {
    double getFahrenheit (String degrees) {
        double celsius = Integer.parseInt(degrees);
        return (celsius * 1.8) + 32;
    }
}
