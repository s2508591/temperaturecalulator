package nl.utwente.di.CelsiusFahrenheit;

import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet(name="hello",urlPatterns={"/hello"})

public class CelsiusFahrenheit extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private Calculator calculator;
    public void init() throws ServletException {
        calculator = new Calculator();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Fahrenheit Calculator";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Degrees in Celsius: " +
                request.getParameter("degrees") + "\n" +
                "  <P>Degrees in Fahrenheit: " +
                Double.toString(calculator.getFahrenheit(request.getParameter("degrees"))) +
                "</BODY></HTML>");
    }
}
